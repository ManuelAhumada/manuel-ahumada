## Formatos de salida

### **nl**

Numera lineas de listas.

### **fold**

Hace el largo de lineas un numero definido.

### **fmt**

Formateador de texto simple.

### **pr**

Prepara texto para ser impreso separandolo en hojas.

### **printf**

Formatea e imprime datos.

### **groff**

Un sistema de formato de documentos.

